# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-headers
# Please be VERY careful upgrading this - vulkan-headers breaks API even
# on point releases. So please make sure everything using this still builds
# after upgrades
pkgver=1.2.169
pkgrel=0
arch="noarch"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan header files"
license="Apache-2.0"
makedepends="cmake"
source="https://github.com/khronosgroup/vulkan-headers/archive/v$pkgver/vulkan-headers-v$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/Vulkan-Headers-$pkgver"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="45e5ff0b514aae62a90a33044375af43b2a8ce98d9073236bc5a987f9c2c4d21be3536445187ac5298d8d39ed322995d509fda5090df3eec008772633b2d2c42  vulkan-headers-v1.2.169.tar.gz"
